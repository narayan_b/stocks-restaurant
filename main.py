import mysql.connector
from mysql_connect import DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_DATABASE
from main_class import Product, RecipeCreate, RestauQt, ReponseQt, StockChange 
from fastapi import Path, Body, FastAPI, HTTPException

app = FastAPI()

# Database connection parameters
db_config = {
    'user': DB_USERNAME,
    'password': DB_PASSWORD,
    'host': DB_HOST,
    'port': DB_PORT,
    'database': DB_DATABASE,
}


# Stock information by restaurant
@app.get("/stock/{product_name}")
def get_stock_info(product_name: str) -> ReponseQt:
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor()

    try:
        # Retrieve stock information for the given product
        query = (
            "SELECT Restaurants.restaurant_name, Stock.quantity "
            "FROM Stock "
            "JOIN Restaurants ON Stock.restaurant_id = Restaurants.restaurant_id "
            "JOIN Products ON Stock.product_id = Products.product_id "
            "WHERE Products.product_name = %s"
        )
        cursor.execute(query, (product_name,))
        result = cursor.fetchall()

        if not result:
            raise HTTPException(status_code=404, detail="Product not found")

        # Calculate total stock
        total_stock = sum(qty for _, qty in result)

        # Format the response using Pydantic models
        response = ReponseQt(
            restaurants=[RestauQt(restaurant_name=name, qt=qty) for name, qty in result],
            total=total_stock
        )
        return response

    finally:
        # Close the database connection
        cursor.close()
        connection.close()


# Create Nex Product
@app.post("/products")
def add_product(product: Product = Body(...)):

    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor()

    try:
        # Check if the product already exists
        check_query = "SELECT product_id FROM Products WHERE product_name = %s"
        cursor.execute(check_query, (product.product_name,))
        existing_product = cursor.fetchone()

        if existing_product:
            raise HTTPException(status_code=400, detail="Product already exists")

        # Insert the new product into the database
        insert_query = "INSERT INTO Products (product_name) VALUES (%s)"
        cursor.execute(insert_query, (product.product_name,))

        connection.commit()

        return {"message": "Product added successfully"}

    finally:
        # Close the database connection
        cursor.close()
        connection.close()




# Get product by name
@app.get("/products/{product_name}")
def get_product(product_name: str = Path(..., description="The name of the product")):
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor()

    try:
        # Retrieve product information for the given product name
        query = "SELECT product_id, product_name FROM Products WHERE product_name = %s"
        cursor.execute(query, (product_name,))
        result = cursor.fetchone()

        if not result:
            raise HTTPException(status_code=404, detail="Product not found")

        # Convert the tuple to a dictionary
        product_dict = dict(zip(cursor.column_names, result))

        # Format the response using Pydantic models
        response = Product(**product_dict)
        return response

    finally:
        # Close the database connection
        cursor.close()
        connection.close()


# Create an endpoint to increase the stock
@app.put("/stock/increase/{product_name}/{restaurant_name}")
def increase_stock(
    product_name: str = Path(..., description="The name of the product"),
    restaurant_name: str = Path(..., description="The name of the restaurant"),
    stock_change: StockChange = Body(..., description="Quantity to increase the stock")
):
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor()

    try:
        # Increase the stock for the given product and restaurant
        update_query = (
            "UPDATE Stock "
            "SET quantity = quantity + %s "
            "WHERE product_id = (SELECT product_id FROM Products WHERE product_name = %s) "
            "AND restaurant_id = (SELECT restaurant_id FROM Restaurants WHERE restaurant_name = %s)"
        )
        cursor.execute(update_query, (stock_change.quantity, product_name, restaurant_name))
        connection.commit()

        return {"message": "Stock increased successfully"}

    finally:
        # Close the database connection
        cursor.close()
        connection.close()



# Create an endpoint to decrease the stock
@app.put("/stock/decrease/{product_name}/{restaurant_name}")
def decrease_stock(
    product_name: str = Path(..., description="The name of the product"),
    restaurant_name: str = Path(..., description="The name of the restaurant"),
    stock_change: StockChange = Body(..., description="Quantity to decrease the stock")
):
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor()

    try:
        # Decrease the stock for the given product and restaurant
        update_query = (
            "UPDATE Stock "
            "SET quantity = GREATEST(0, quantity - %s) "
            "WHERE product_id = (SELECT product_id FROM Products WHERE product_name = %s) "
            "AND restaurant_id = (SELECT restaurant_id FROM Restaurants WHERE restaurant_name = %s)"
        )
        cursor.execute(update_query, (stock_change.quantity, product_name, restaurant_name))
        connection.commit()

        return {"message": "Stock decreased successfully"}

    finally:
        # Close the database connection
        cursor.close()
        connection.close()



# Endpoint to add a recipe
@app.post("/recipes")
def add_recipe(recipe: RecipeCreate = Body(...)):
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor()

    try:
        # Check if the recipe already exists
        check_query = "SELECT recipe_id FROM Recipes WHERE recipe_name = %s"
        cursor.execute(check_query, (recipe.recipe_name,))
        existing_recipe = cursor.fetchone()

        if existing_recipe:
            raise HTTPException(status_code=400, detail="Recipe already exists")

        # Initialize a dictionary to store product_id for each ingredient
        ingredient_product_ids = {}

        # Check if each ingredient exists in the Products table
        for ingredient in recipe.ingredients:
            check_product_query = "SELECT product_id FROM Products WHERE product_name = %s"
            cursor.execute(check_product_query, (ingredient,))
            existing_product = cursor.fetchone()

            if existing_product:
                # If the product exists, use its product_id
                ingredient_product_ids[ingredient] = existing_product[0]
            else:
                # If the product doesn't exist, add it as a new product
                insert_product_query = "INSERT INTO Products (product_name) VALUES (%s)"
                cursor.execute(insert_product_query, (ingredient,))
                connection.commit()

                # Retrieve the product_id of the newly added product
                cursor.execute(check_product_query, (ingredient,))
                new_product_id = cursor.fetchone()[0]
                ingredient_product_ids[ingredient] = new_product_id

        # Insert the new recipe into the Recipes table
        insert_recipe_query = "INSERT INTO Recipes (recipe_name) VALUES (%s)"
        cursor.execute(insert_recipe_query, (recipe.recipe_name,))
        connection.commit()

        # Get the recipe_id of the newly added recipe
        get_recipe_id_query = "SELECT recipe_id FROM Recipes WHERE recipe_name = %s"
        cursor.execute(get_recipe_id_query, (recipe.recipe_name,))
        recipe_id = cursor.fetchone()[0]

        # Insert ingredients and quantities into the RecipeIngredients table
        insert_ingredients_query = "INSERT INTO RecipeIngredients (recipe_id, product_id, quantity) VALUES (%s, %s, %s)"
        for ingredient, quantity in zip(recipe.ingredients, recipe.quantities):
            cursor.execute(insert_ingredients_query, (recipe_id, ingredient_product_ids[ingredient], quantity))
        
        connection.commit()

        return {"message": "Recipe added successfully"}

    finally:
        # Close the database connection
        cursor.close()
        connection.close()


# Endpoint to get a recipe by name with associated products and quantities
@app.get("/recipes/{recipe_name}")
def get_recipe_by_name(recipe_name: str = Path(..., title="The name of the recipe")):
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor(dictionary=True)

    try:
        # Retrieve the recipe and associated products by name
        query = """
        SELECT Recipes.recipe_id, Recipes.recipe_name, Products.product_name, RecipeIngredients.quantity
        FROM Recipes
        JOIN RecipeIngredients ON Recipes.recipe_id = RecipeIngredients.recipe_id
        JOIN Products ON RecipeIngredients.product_id = Products.product_id
        WHERE Recipes.recipe_name = %s
        """
        cursor.execute(query, (recipe_name,))
        recipe_info = cursor.fetchall()

        if not recipe_info:
            raise HTTPException(status_code=404, detail="Recipe not found")

        # Transform the result into a dictionary with the desired structure
        recipe_dict = {
            "recipe_id": recipe_info[0]["recipe_id"],
            "recipe_name": recipe_info[0]["recipe_name"],
            "quantity": recipe_info[0]["quantity"],
            "products": [{"product_name": item["product_name"], "quantity": item["quantity"]} for item in recipe_info],
        }

        return recipe_dict

    finally:
        # Close the database connection
        cursor.close()
        connection.close()


#######################################################
        

# # Helper function to execute a query
# def execute_query(query, params=None, fetchall=False):
#     try:
#         with connect(**DATABASE_CONFIG) as connection:
#             with connection.cursor(dictionary=True) as cursor:
#                 cursor.execute(query, params)
#                 if fetchall:
#                     return cursor.fetchall()
#                 return cursor.fetchone()
#     except Error as e:
#         print(e)
#         raise HTTPException(status_code=500, detail="Database error")

# # Pydantic models for request and response
# class RecipeRequest(BaseModel):
#     recipe_name: str

# class RecipeResult(BaseModel):
#     restaurant_name: str
#     stock_quantity: int
#     product_name: str
#     result_quantity: int

# # FastAPI endpoint
# @app.post("/check_recipe_availability", response_model=List[RecipeResult])
# def check_recipe_availability(recipe_request: RecipeRequest):
#     try:
#         # Retrieve recipe information
#         recipe_name = recipe_request.recipe_name
#         query = "SELECT * FROM Recipes WHERE recipe_name = %s"
#         recipe = execute_query(query, (recipe_name,))

#         if not recipe:
#             raise HTTPException(status_code=404, detail="Recipe not found")

#         # Check stock availability for each ingredient
#         results = []
#         for ingredient in recipe["ingredients"]:
#             product_name = ingredient["product_name"]
#             required_quantity = ingredient["quantity"]

#             # Retrieve stock information
#             query = """
#                 SELECT s.*, r.restaurant_name, p.product_name
#                 FROM Stock s
#                 JOIN Restaurants r ON s.restaurant_id = r.restaurant_id
#                 JOIN Products p ON s.product_id = p.product_id
#                 WHERE s.restaurant_id = 1 AND s.product_id = %s
#             """
#             stock = execute_query(query, (ingredient["product_id"],))

#             if stock:
#                 stock_quantity = stock["quantity"]

#                 # Check if stock is sufficient
#                 if stock_quantity >= required_quantity:
#                     # Calculate the number of times the ingredient can be used
#                     result_quantity = stock_quantity // required_quantity

#                     # Append result to the list
#                     results.append({
#                         'restaurant_name': stock["restaurant_name"],
#                         'stock_quantity': stock_quantity,
#                         'product_name': product_name,
#                         'result_quantity': result_quantity
#                     })
#                 else:
#                     results.append({
#                         'restaurant_name': stock["restaurant_name"],
#                         'stock_quantity': stock_quantity,
#                         'product_name': product_name,
#                         'result_quantity': 0  # Indicate insufficient stock
#                     })

#         return results
#     except HTTPException as e:
#         raise e
#     except Exception as e:
#         print(e)
#         raise HTTPException(status_code=500, detail="Server error")



#########################################################""



# Get all products
@app.get("/products")
def get_all_products():
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor()

    try:
        # Retrieve all products
        query = "SELECT product_id, product_name FROM Products"
        cursor.execute(query)
        results = cursor.fetchall()

        if not results:
            raise HTTPException(status_code=404, detail="No products found")

        # Convert the list of tuples to a list of dictionaries
        products_list = [dict(zip(cursor.column_names, result)) for result in results]

        # Format the response using Pydantic models
        response = [Product(**product_dict) for product_dict in products_list]
        return response

    finally:
        # Close the database connection
        cursor.close()
        connection.close()


# Endpoint to get all recipes
@app.get("/recipes")
def get_all_recipes():
    # Connect to the database
    connection = mysql.connector.connect(**db_config)
    cursor = connection.cursor(dictionary=True)

    # Retrieve all recipes
    cursor.execute("SELECT recipe_id, recipe_name FROM Recipes")
    recipes = cursor.fetchall()

    if not recipes:
        raise HTTPException(status_code=404, detail="No recipes found")

    # Close the database connection
    cursor.close()
    connection.close()

    return recipes



if __name__ == "__main__":
    product_name = "pain"
    stock_info = get_stock_info(product_name)
    print(stock_info.model_dump_json())

