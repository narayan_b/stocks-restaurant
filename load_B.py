import pandas as pd
import mysql.connector
from unidecode import unidecode
from mysql_connect import DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_DATABASE

# Load data from extract_B.csv
restaurant_name_B = 'extract_B'
data_B = pd.read_csv(f'docs/{restaurant_name_B}.csv')

# Convert 'inventory' column to numeric type
data_B['inventory'] = pd.to_numeric(data_B['inventory'], errors='coerce')

# Convert 'date' column to datetime format
data_B['date'] = pd.to_datetime(data_B['date'], dayfirst=True, errors='coerce')

# Define a function for text normalization using unidecode
def normalize_text(text):
    # Convert to lowercase, remove 's' from the end of words, and remove accents
    return " ".join(word.rstrip('s') for word in unidecode(text).split()).lower()

# Apply the text normalization function to a relevant column in data_B
# Replace 'stock' with the actual column name you want to normalize
data_B['stock'] = data_B['stock'].apply(normalize_text)

# Check for duplicate stocks and merge them, keeping the latest date
# Check for duplicate stocks and merge them, keeping the latest date
merged_data_B = data_B.groupby('stock')[['inventory', 'date']].max().reset_index()

print(merged_data_B)

# MySQL database connection using variables from mysql_connect.py
conn_B = mysql.connector.connect(
    user=DB_USERNAME,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT,
    database=DB_DATABASE
)
cursor_B = conn_B.cursor(buffered=True)  # Use buffered cursor without multi=True

# Insert or update the restaurant in the 'Restaurants' table
cursor_B.execute("INSERT INTO Restaurants (restaurant_name) VALUES (%s) ON DUPLICATE KEY UPDATE restaurant_name=restaurant_name", (restaurant_name_B,))

# Get the restaurant_id for the inserted/updated restaurant
restaurant_id_B_query = "SELECT restaurant_id FROM Restaurants WHERE restaurant_name = %s"
cursor_B.execute(restaurant_id_B_query, (restaurant_name_B,))
restaurant_id_B = cursor_B.fetchone()[0]


# Check if the connection is open before executing queries
if not conn_B.is_connected():
    conn_B.reconnect()

# Iterate over the DataFrame and insert or update data into the database
for index, row in merged_data_B.iterrows():
    # Check if the product already exists in the 'Products' table
    cursor_B.execute("SELECT product_id FROM Products WHERE product_name = %s", (row['stock'],))
    product_id_B_result = cursor_B.fetchone()

    if product_id_B_result:
        # If the product exists, use the existing product_id
        product_id_B = product_id_B_result[0]
    else:
        # If the product does not exist, insert it into the 'Products' table
        cursor_B.execute("INSERT INTO Products (product_name) VALUES (%s)", (row['stock'],))
        conn_B.commit()
        product_id_B = cursor_B.lastrowid

    # Check if the restaurant already exists in the 'Restaurants' table
    cursor_B.execute("SELECT restaurant_id FROM Restaurants WHERE restaurant_name = %s", (restaurant_name_B,))
    restaurant_id_B_result = cursor_B.fetchone()

    if restaurant_id_B_result:
        # If the restaurant exists, use the existing restaurant_id
        restaurant_id_B = restaurant_id_B_result[0]
    else:
        # If the restaurant does not exist, insert it into the 'Restaurants' table
        cursor_B.execute("INSERT INTO Restaurants (restaurant_name) VALUES (%s)", (restaurant_name_B,))
        conn_B.commit()
        restaurant_id_B = cursor_B.lastrowid

    # Insert or update data into the 'Stock' table with the correct restaurant_id and product_id
    cursor_B.execute("INSERT INTO Stock (restaurant_id, product_id, quantity, date) VALUES (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE quantity = VALUES(quantity), date = VALUES(date)",
                     (restaurant_id_B, product_id_B, row['inventory'], row['date']))

# Commit and close the connection
conn_B.commit()
conn_B.close()