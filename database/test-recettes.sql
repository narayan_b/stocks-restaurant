USE restaurant_stocks;
-- Insert test recipes with available and unavailable products
INSERT INTO Recipes (recipe_name) VALUES
    ('Pancakes'), ('Chocolate Cake');

-- Insert ingredients for Pancakes recipe using existing products
INSERT INTO RecipeIngredients (recipe_id, product_id, quantity) VALUES
    (6, (SELECT product_id FROM Products WHERE product_name = 'Farine'), 2),
    (6, (SELECT product_id FROM Products WHERE product_name = 'Sucre roux'), 1),
    (6, (SELECT product_id FROM Products WHERE product_name = 'Oeuf'), 3);

-- Insert ingredients for Chocolate Cake recipe using existing products
INSERT INTO RecipeIngredients (recipe_id, product_id, quantity) VALUES
    (7, (SELECT product_id FROM Products WHERE product_name = 'Farine'), 3),
    (7, (SELECT product_id FROM Products WHERE product_name = 'Sucre roux'), 2),
    (7, (SELECT product_id FROM Products WHERE product_name = 'chocolat'), 1),
    (7, (SELECT product_id FROM Products WHERE product_name = 'Beurre'), 2);
