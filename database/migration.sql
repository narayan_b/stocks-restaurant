USE restaurant_stocks;

-- Add a new column to the Stock table for date information
ALTER TABLE Stock
ADD COLUMN date DATE DEFAULT '2023-01-01'; -- add a default value

-- Table pour les recettes
CREATE TABLE Recipes (
    recipe_id INT PRIMARY KEY AUTO_INCREMENT,
    recipe_name VARCHAR(255) NOT NULL
);

-- Table pour les ingrédients des recettes
CREATE TABLE RecipeIngredients (
    recipe_id INT,
    product_id INT,
    quantity INT,
    PRIMARY KEY (recipe_id, product_id),
    FOREIGN KEY (recipe_id) REFERENCES Recipes(recipe_id),
    FOREIGN KEY (product_id) REFERENCES Products(product_id)
);

grant all privileges on *.* to 'narayan123'@'localhost';