-- Drop and create the database if needed
DROP DATABASE IF EXISTS restaurant_stocks;
CREATE DATABASE IF NOT EXISTS restaurant_stocks;
USE restaurant_stocks;

-- Table for Products
CREATE TABLE Products (
    product_id INT PRIMARY KEY AUTO_INCREMENT,
    product_name VARCHAR(255) NOT NULL
);

-- Table for Restaurants
CREATE TABLE Restaurants (
    restaurant_id INT PRIMARY KEY AUTO_INCREMENT,
    restaurant_name VARCHAR(255) NOT NULL
);

-- Table for Stock
CREATE TABLE Stock (
    stock_id INT PRIMARY KEY AUTO_INCREMENT,
    restaurant_id INT,
    product_id INT,
    quantity INT,
    FOREIGN KEY (restaurant_id) REFERENCES Restaurants(restaurant_id),
    FOREIGN KEY (product_id) REFERENCES Products(product_id)
);


grant all privileges on *.* to 'narayan123'@'localhost';
