import pandas as pd  # Importing the pandas library and using as 'pd' for shortcut.
import mysql.connector # Importing the mysql.connector library for connecting to MySQL databases.
from unidecode import unidecode # Importing the unidecode library for handling Unicode text
from mysql_connect import DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_DATABASE # Importing specific variables from the mysql_connect.py file for connect with mysql.

# Store file name in variable restaurant_name.
restaurant_name = 'extract_A'
# Load data from extract_A.csv
data_A = pd.read_csv(f'docs/{restaurant_name}.csv')

# Convert 'stock' column to numeric type
data_A['stock'] = pd.to_numeric(data_A['stock'], errors='coerce')

# Define a function for text normalization using unidecode
def normalize_text(text):
    # Convert to lowercase, remove 's' from the end of words, and remove accents
    return " ".join(word.rstrip('s') for word in unidecode(text).split()).lower()

# Apply the text normalization function to the 'product' column
data_A['product'] = data_A['product'].apply(normalize_text)

# Check for duplicate products and merge them
merged_data = data_A.groupby('product')['stock'].sum().reset_index()

# Sort the merged data by 'product' in alphabetical order
merged_data = merged_data.sort_values(by='product')

# Check dataframe is working well or not
# print(merged_data)


# MySQL database connection using variables from mysql_connect.py
conn = mysql.connector.connect( # create mysql connector and store in conn variable.
    user=DB_USERNAME,           # Database username
    password=DB_PASSWORD,       # Database password
    host=DB_HOST,               # Database host
    port=DB_PORT,               # Database port
    database=DB_DATABASE        # Database database
)
cursor = conn.cursor()          # Create a cursor object to interact with the database

# Insert or update the restaurant in the 'Restaurants' table
cursor.execute("INSERT INTO Restaurants (restaurant_name) VALUES (%s) ON DUPLICATE KEY UPDATE restaurant_name=restaurant_name", (restaurant_name,))

# Get the restaurant_id for the inserted/updated restaurant
restaurant_id_query = "SELECT restaurant_id FROM Restaurants WHERE restaurant_name = %s"    # Define a SQL query to retrieve the 'restaurant_id' for a given 'restaurant_name' in the 'Restaurants' table
cursor.execute(restaurant_id_query, (restaurant_name,))     # Execute the SQL query with the 'restaurant_name' as a parameter
restaurant_id = cursor.fetchone()[0]        # Fetch the restaurant_id from the query result


# Iterate over the DataFrame and insert data into the database
for index, row in merged_data.iterrows():   # Loop through each row in the DataFrame
    # Insert or update the product in the 'Products' table
    cursor.execute("INSERT INTO Products (product_name) VALUES (%s) ON DUPLICATE KEY UPDATE product_name=product_name", (row['product'],))

    # Get the product_id for the inserted/updated product
    product_id_query = "SELECT product_id FROM Products WHERE product_name = %s" # SQL query to fetch 'product_id' based on 'product_name'
    cursor.execute(product_id_query, (row['product'],)) # Execute the query with the current 'product_name'
    product_id = cursor.fetchone()[0] # Fetch the 'product_id' from the query result

    # Insert data into the 'Stock' table with the correct restaurant_id
    cursor.execute("INSERT INTO Stock (restaurant_id, product_id, quantity) VALUES (%s, %s, %s)", (restaurant_id, product_id, row['stock']))

# Commit and close the connection
conn.commit()  # Save the changes made to the database
# Close the database connection
conn.close() # Disconnect from the database
