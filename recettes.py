import pandas as pd
import mysql.connector
from unidecode import unidecode
from mysql_connect import DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_DATABASE

# Load data from the CSV file
data_recipes = pd.read_csv('docs/recettes.csv')

# Convert 'spoon' column to numeric type
data_recipes['spoon'] = pd.to_numeric(data_recipes['spoon'], errors='coerce')

# Define a function for text normalization using unidecode
def normalize_text(text):
    # Convert to lowercase, remove 's' from the end of words, and remove accents
    return " ".join(word.rstrip('s') for word in unidecode(text).split()).lower()

# Apply the text normalization function to the 'ingredient' column
data_recipes['ingredient'] = data_recipes['ingredient'].apply(normalize_text)

# Remove double quotes from the 'name' column
data_recipes['name'] = data_recipes['name'].str.replace('"', '')

# Display the updated DataFrame
print(data_recipes)

# Establish a database connection
db_connection = mysql.connector.connect(
    host=DB_HOST,
    user=DB_USERNAME,
    password=DB_PASSWORD,
    port=DB_PORT,
    database=DB_DATABASE
)
cursor = db_connection.cursor()

# Iterate through each row in the DataFrame and insert data into the database
for index, row in data_recipes.iterrows():
    ingredient = row['ingredient'].strip()
    spoon = int(row['spoon'])
    recipe_name = row['name'].strip() 

    # Check if the recipe already exists in the Recipes table
    cursor.execute("SELECT recipe_id FROM Recipes WHERE recipe_name = %s", (recipe_name,))
    result = cursor.fetchone()

    if result:
        recipe_id = result[0]
    else:
        # If the recipe doesn't exist, insert it into the Recipes table
        cursor.execute("INSERT INTO Recipes (recipe_name) VALUES (%s)", (recipe_name,))
        db_connection.commit()
        recipe_id = cursor.lastrowid

    # Consume any unread result
    cursor.fetchall()

    # Check if the ingredient already exists in the Products table
    cursor.execute("SELECT product_id FROM Products WHERE product_name = %s", (ingredient,))
    result = cursor.fetchone()

    if result:
        product_id = result[0]
    else:
        # If the ingredient doesn't exist, insert it into the Products table
        cursor.execute("INSERT INTO Products (product_name) VALUES (%s)", (ingredient,))
        db_connection.commit()
        product_id = cursor.lastrowid

    # Consume any unread result
    cursor.fetchall()

    # Check if the entry already exists in RecipeIngredients table
    cursor.execute("SELECT * FROM RecipeIngredients WHERE recipe_id = %s AND product_id = %s", (recipe_id, product_id))
    existing_entry = cursor.fetchone()

    if not existing_entry:
        # If the entry doesn't exist, insert it into the RecipeIngredients table
        cursor.execute("INSERT INTO RecipeIngredients (recipe_id, product_id, quantity) VALUES (%s, %s, %s)", (recipe_id, product_id, spoon))
        db_connection.commit()

# Close the database connection
cursor.close()
db_connection.close()
