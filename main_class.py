from typing import List, Optional
from pydantic import BaseModel

class RestauQt(BaseModel):
    restaurant_name: str
    qt: int

class ReponseQt(BaseModel):
    restaurants: List[RestauQt]
    total: int

class Product(BaseModel):
    product_id: Optional[int] = None
    product_name: str

class StockChange(BaseModel):
    quantity: int

class RecipeCreate(BaseModel):
    recipe_name: str
    ingredients: list[str]
    quantities: list[str]

class Recipe(BaseModel):
    recipe_id: int
    recipe_name: str
    ingredients: str
    quantities: List[str]
