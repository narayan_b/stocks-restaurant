import pandas as pd
import mysql.connector
from unidecode import unidecode
from mysql_connect import DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_DATABASE

# Load data from extract_C.csv
restaurant_name_C = 'extract_C'
data_C = pd.read_csv(f'docs/{restaurant_name_C}.csv', names=['product', 'quantity'])

# Convert 'quantity' column to numeric type
data_C['quantity'] = pd.to_numeric(data_C['quantity'], errors='coerce')

# Convert negative values to 0
data_C['quantity'] = data_C['quantity'].apply(lambda x: max(0, x))

# Define a function for text normalization using unidecode
def normalize_text(text):
    # Convert to lowercase, remove 's' from the end of words, and remove accents
    return " ".join(word.rstrip('s') for word in unidecode(text).split()).lower()

# Apply the text normalization function to the 'product' column
data_C['product'] = data_C['product'].apply(normalize_text)

# Group by product name and sum the quantities
grouped_data_C = data_C.groupby('product')['quantity'].sum().reset_index()

# Sort the grouped data by 'product' in alphabetical order
grouped_data_C = grouped_data_C.sort_values(by='product')

print(grouped_data_C)

# MySQL database connection using variables from mysql_connect.py
conn_C = mysql.connector.connect(
    user=DB_USERNAME,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT,
    database=DB_DATABASE
)
cursor_C = conn_C.cursor(buffered=True)  # Use buffered cursor without multi=True

# Insert or update the restaurant in the 'Restaurants' table
cursor_C.execute("INSERT INTO Restaurants (restaurant_name) VALUES (%s) ON DUPLICATE KEY UPDATE restaurant_name=restaurant_name", (restaurant_name_C,))

# Get the restaurant_id for the inserted/updated restaurant
restaurant_id_C_query = "SELECT restaurant_id FROM Restaurants WHERE restaurant_name = %s"
cursor_C.execute(restaurant_id_C_query, (restaurant_name_C,))
restaurant_id_C_result = cursor_C.fetchone()

if restaurant_id_C_result:
    # If the restaurant exists, use the existing restaurant_id
    restaurant_id_C = restaurant_id_C_result[0]
else:
    # If the restaurant does not exist, insert it into the 'Restaurants' table
    cursor_C.execute("INSERT INTO Restaurants (restaurant_name) VALUES (%s)", (restaurant_name_C,))
    conn_C.commit()
    restaurant_id_C = cursor_C.lastrowid

# Iterate over the DataFrame and insert or update data into the database
for index, row in grouped_data_C.iterrows():
    # Check if the product already exists in the 'Products' table
    cursor_C.execute("SELECT product_id FROM Products WHERE product_name = %s", (row['product'],))
    product_id_C_result = cursor_C.fetchone()

    if product_id_C_result:
        # If the product exists, use the existing product_id
        product_id_C = product_id_C_result[0]
    else:
        # If the product does not exist, insert it into the 'Products' table
        cursor_C.execute("INSERT INTO Products (product_name) VALUES (%s)", (row['product'],))
        conn_C.commit()
        product_id_C = cursor_C.lastrowid

    # Insert data into the 'Stock' table with the correct restaurant_id and product_id
    cursor_C.execute("INSERT INTO Stock (restaurant_id, product_id, quantity) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE quantity = VALUES(quantity)", (restaurant_id_C, product_id_C, row['quantity']))

# Commit and close the connection
conn_C.commit()
conn_C.close()

